package com.example.greetings

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class GreetingTest {

    private val greeting = Greeting()

    @Test
    @DisplayName("Test if method returns name in greeting string")
    fun requirement1() {
        //given
        val name = "Bob"
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("Hello, Bob.", message)
    }

    @Test
    @DisplayName("Test when name is null")
    fun requirement2(){
        //given
        val name: String? = null
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("Hello, my friend.", message)
    }

    @Test
    @DisplayName("Test when the name is in all uppercase")
    fun requirement3() {
        //given
        val name = "BOB"
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("HELLO BOB!", message)
    }

    @Test
    @DisplayName("Test when the name is 2 names")
    fun requirement4() {
        //given
        val name = arrayOf("Jill", "Jane")
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("Hello, ${name[0]} and ${name[1]}.", message)
    }

    @Test
    @DisplayName("Test when the name is 3 names")
    fun requirement5() {
        //given
        val name = arrayOf("Jill", "Jane", "Judith")
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("Hello, ${name[0]}, ${name[1]}, and ${name[2]}.", message)
    }

    @Test
    @DisplayName("Test when the name is 2 normal names and 1 shouted")
    fun requirement6() {
        //given
        val name = arrayOf("Jill", "JANE", "Judith")
        //when
        val message = greeting.greet(name)
        //then
        Assertions.assertEquals("Hello, ${name[0]} and ${name[2]}. AND HELLO ${name[1]}!", message)
    }
}