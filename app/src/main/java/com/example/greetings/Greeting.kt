package com.example.greetings

class Greeting {
    fun greet(name: String?): String
    {
        val updatedName = name ?: "my friend"
        if(updatedName == updatedName.uppercase()) return "HELLO $updatedName!"
        return "Hello, $updatedName."
    }
    fun greet(names: Array<String>): String {
        val (lowercaseNames, shoutnames)  = names.partition { it != it.uppercase() }
        var resultString : String = ""
        if (lowercaseNames.isNotEmpty())
        {
            resultString += lowercaseGreet(lowercaseNames.toTypedArray())
            if (shoutnames.isNotEmpty()) resultString += " AND " + upperCaseGreet(shoutnames.toTypedArray())
        }
        else if(shoutnames.isNotEmpty()) resultString += upperCaseGreet(shoutnames.toTypedArray())
        else resultString = greet(null)
        return resultString
    }

    fun lowercaseGreet(names: Array<String>): String {
        var result: String = ""
        return when (names.size) {
            1 -> greet(names[0])
            2 -> greet("${names[0]} and ${names[1]}")
            else -> {
                for (name: String in names) {
                    result += if (name == names.last()) "and $name"
                    else "$name, "
                }
                greet(result)
            }
        }
    }
    fun upperCaseGreet(names: Array<String>): String {
        var result: String = ""
        return when (names.size) {
            1 -> greet(names[0])
            2 -> greet("${names[0]} AND ${names[1]}")
            else -> {
                for (name: String in names) {
                    result += if (name == names.last()) "AND $name"
                    else "$name "
                }
                greet(result)
            }
        }
    }
}